import Joi from 'joi';
import badRequest from '../utils/errors/badRequest'

const schema = Joi.object({
  name: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string().required().min(8),
});

export default (req, res, next) => {
  const { error } = schema.validate(req.body);
  if (error) {
    throw new badRequest(error.details[0].message, 400);
  }
  next();
};
