import express from 'express'
import user from './api/user'
import todo from './api/todo'

const routes = express.Router()

routes.use('/user', user)
routes.use('/todo', todo)

export default routes