import { hashPassword } from '../utils/auth'

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User',{
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    refreshtoken: DataTypes.STRING
  });


  User.associate = function(models) {
    User.hasMany(models.Todo, {
      foreignKey: 'userId',
      as: 'todos',
      onDelete: 'CASCADE'
    })
  }

  User.beforeCreate((user) => {
    if (user.password) { user.password = hashPassword(user.password); }
  });


  return User;
};