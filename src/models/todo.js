module.exports = (sequelize, DataTypes) => {
  const Todo = sequelize.define('Todo',{
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    task: DataTypes.STRING,
    completed: {
      type: DataTypes.BOOLEAN,
      defaultValue: false

    },
    userId: DataTypes.UUID
  });

  Todo.associate = function(models){
    Todo.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'creator',
      onDelete: 'CASCADE'
    })
  }

  return Todo
}