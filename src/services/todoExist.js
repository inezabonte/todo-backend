import models from '../models'

const todoExist = async (data) => {
    const todo = await models.Todo.findOne({ where: {id: data} })
    return todo
}

export default todoExist