import express from 'express'
import { createTodo , getTodo, updateTodo, deleteTodo} from '../../controllers/todo'
import verifyToken from '../../middlewares/verifyToken'
import taskVerification from '../../middlewares/taskVerfication'

const router = express.Router()

router.get('/', verifyToken, getTodo)
router.post('/', verifyToken, createTodo)
router.patch('/', verifyToken, taskVerification, updateTodo)
router.delete('/', verifyToken, taskVerification, deleteTodo)

export default router