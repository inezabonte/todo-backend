import models from '../models'
import badRequest from '../utils/errors/badRequest'
import userExist from '../services/userExist'
import 'express-async-errors'

const signup = async (req, res, next) => {
    const exists = await userExist(req.body.email)

    if(exists){
        throw new badRequest('Account already exists')
    }

    try {
        await models.User.create(req.body);
        res.status(201).json({status: 201, message: "User Created successfully"})
    } catch (error) {
        next(error)
    }

}

export default signup