import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import notAuthorized from '../utils/errors/authorizationError'
import 'express-async-errors'

const secret = process.env.TOKEN_SECRET;


export const hashPassword = (password) => bcrypt.hashSync(password, 10);

export const compareBcrypt = (password, hash) => bcrypt.compare(password,hash);

export const generateToken = (payload, expiresIn = '7d') => (
    jwt.sign({...payload}, secret,{expiresIn})
)

export const verifyToken = (token) => {
    try {
        return jwt.verify(token, process.env.TOKEN_SECRET)
    } catch (error) {
        throw new notAuthorized(error.message)
    }
   
    
  };