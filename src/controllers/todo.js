import models from '../models'
import 'express-async-errors'

export const getTodo = async (req, res, next) => {

    const userId = req.currentUser.userId

    try {
        const tasks = await models.Todo.findAll({where:{userId}})
        res.status(200).json({todos: tasks})
    } catch (error) {
        next(error)
    }
}

export const createTodo = async (req, res, next) => {
    

    req.body.userId = req.currentUser.userId

    try {
        const createTask = await models.Todo.create(req.body);
        res.status(201).json({status: 201, task: createTask})
    } catch (error) {
        next(error)
    }
}

export const updateTodo = async (req, res, next) => {

    const userId = req.currentUser.userId
    const {taskId} = req.query

    try {
        await models.Todo.update(req.body, {where: {id:taskId, userId}})
        res.status(200).json({status: 200, message: "Task updated successfully"})
    } catch (error) {
        next(error)
    }
}

export const deleteTodo = async (req, res, next) => {

    const userId = req.currentUser.userId
    const {taskId} = req.query

    try {
        await models.Todo.destroy({where: {id: taskId, userId}})
        res.status(200).json({status: 200, message: "Task deleted successfully"})
    } catch (error) {
        next(error)
    }
}