import 'express-async-errors'
import notFound from '../utils/errors/notFound'
import badRequest from '../utils/errors/badRequest'
import userExist from '../services/userExist'
import { compareBcrypt, generateToken } from '../utils/auth'

const login = async(req, res, next) => {
    const {email, password} = req.body;
    const exist = await userExist(email)

    if(!exist){
       throw new notFound("Credentials incorrect, please try again")
    }

    const checkPassword = await compareBcrypt(password, exist.password)
    if(!checkPassword){
        throw new badRequest("Credentials incorrect, please try again")
    }

    try {
        const userToken = await generateToken({userId: exist.id, email})
        await exist.update({ refreshtoken: userToken });
        return res.status(200).json({status: 200, message: "login successful", userToken})
    } catch (error) {
        next(error)
    }

    
}

export default login