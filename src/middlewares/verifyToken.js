import { verifyToken } from '../utils/auth';
import AuthorizationError from '../utils/errors/authorizationError';
import userExist from '../services/userExist'
import 'express-async-errors'

const verifyUserToken = async (req, res, next) => {
    if (!req.headers.authorization) throw new AuthorizationError('no auth header found');

    const authHeader = req.headers.authorization.split(' ');
    const [, token] = authHeader;

    if (!token) throw new AuthorizationError('No token found');
    const decodedToken = await verifyToken(token);

    const record = await userExist(decodedToken.email)
    if (!record) throw new AuthorizationError('Data in token is invalid');
    
    req.currentUser = decodedToken
    next();
};
export default verifyUserToken;
