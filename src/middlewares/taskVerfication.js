import notFound from '../utils/errors/notFound'
import notAuthorized from '../utils/errors/authorizationError'
import todoExist from '../services/todoExist'
import 'express-async-errors'

const taskVerification = async (req, res, next) => {
    const {taskId} = req.query
    const {userId} = req.currentUser

    const exists = await todoExist(taskId)
    if(!exists){
        throw new notFound("Could'nt find task")
    }

    if(exists.userId != userId){
        throw new notAuthorized("Task does not belong to you")
    }

    next()

}

export default taskVerification