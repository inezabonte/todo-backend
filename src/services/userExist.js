import models from '../models'

const userExist = async (data) => {
    const user = await models.User.findOne({ where: {email: data} })
    return user
}

export default userExist