import express from 'express'
import signupValidation from '../../middlewares/signupValidation'
import signupController from '../../controllers/signup'
import loginController from '../../controllers/login'
import loginValidation from '../../middlewares/loginValidation'

const router = express.Router()

router.post('/signup', signupValidation, signupController)
router.post('/login', loginValidation, loginController )

export default router